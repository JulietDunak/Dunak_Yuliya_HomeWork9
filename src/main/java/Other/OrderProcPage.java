package Other;
import Utils.Click;
import Utils.MyWaits;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
/**
 * Created by Home on 12.10.2017.
 */
public class OrderProcPage {
  Click click;
  MyWaits myWaits;
  private final WebDriver driver;
  public OrderProcPage(WebDriver driver) {
    this.driver = driver;
    myWaits = new MyWaits(driver);
    click= new Click(driver);
      }
  @FindBy(xpath=".//*[@id='products']/li[1]")
  WebElement product;
  @FindBy(xpath = (".//*[contains(@class,'add-to-cart js-button')]"))
  WebElement addtocart;
  @FindBy(id="cart-summary-checkout")
  WebElement checkout;

  public void chekout() {
    driver.get("https://www.templatemonster.com/ua/html-templates/62319.html");
    myWaits.waitClickHeart();
    click.methodClick(addtocart);
    myWaits.waitPriceBlock();
    click.methodClick(checkout);
     }
}