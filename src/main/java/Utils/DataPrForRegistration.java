package Utils;
import org.testng.annotations.DataProvider;

/**
 * Created by Home on 14.10.2017.
 */
public class DataPrForRegistration {
  @DataProvider(name = "data-provider")
  public Object[][] path() {
    return new Object[][]{
            {"src/test/resources/UkrainianUser.properties"},
            {"src/test/resources/USUser.properties"},
    };
  }
}
