import Other.OrderProcPage;
import Account.RegistrationPage;
import Utils.*;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class RegistrationTest {
  static WebDriver driver;
  private static MyWaits wait;
  @BeforeClass
  void setup() {
    System.setProperty("webdriver.chrome.driver", "C:\\Users\\Yuliya\\Desktop\\setup\\chromedriver.exe");
    //System.setProperty("webdriver.chrome.driver", "C:\\Users\\Home\\Downloads\\chromedriver.exe");
    //System.setProperty("webdriver.chrome.driver", "C:\\Users\\it-school\\Desktop\\setup\\chromedriver.exe");
    driver = (WebDriver) new ChromeDriver();
    // driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
    driver.get("https://www.templatemonster.com/");
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    wait = new MyWaits(driver);
  }
  @BeforeMethod
  void Buy(){
    OrderProcPage orderProcPage = PageFactory.initElements(driver, OrderProcPage.class);
    orderProcPage.chekout();
  }
  @Test(dataProvider = "data-provider", dataProviderClass = DataPrForRegistration.class)//(dataProvider = "data-provider", dataProviderClass = DataPrForRegistration.class)
  public void registrationTest(String path){
    RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
    User user=new User(path);
    registrationPage.registerAccount(user);
    wait.waitPayment();
    Assert.assertTrue(registrationPage.isPaymentMethod());
          }

//  public void screenshotFailure(String filename) throws IOException {
//    TakesScreenshot scrShot =((TakesScreenshot)driver);
//    File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
//    //File targetFile=new File("./Screenshots/Failure/" + manager.helperBase.generateCurrentDate() + "/" + filename +".jpg");
//    FileUtils.copyFile(SrcFile,targetFile);
//  }

  @AfterMethod
  void deleteCookies(){
    driver.manage().deleteAllCookies();
  }
  @AfterClass
  public void tearDown() {
    driver.quit();
  }
  }

